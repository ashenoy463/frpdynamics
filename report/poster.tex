% Unofficial University of Cambridge Poster Template
% https://github.com/andiac/gemini-cam
% a fork of https://github.com/anishathalye/gemini
% also refer to https://github.com/k4rtik/uchicago-poster

\documentclass[final]{beamer}

% ====================
% Packages
% ====================

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[height=1,width=1,scale=1.0]{beamerposter}
\usetheme{gemini}
\usecolortheme{nott}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.14}
\usepackage{anyfontsize}
\usepackage{caption,subcaption}
\geometry{papersize={100cm,100cm}}
% ====================
% Lengths
% ====================

% If you have N columns, choose \sepwidth and \colwidth such that
% (N+1)*\sepwidth + N*\colwidth = \paperwidth
\newlength{\sepwidth}
\newlength{\colwidth}
\setlength{\sepwidth}{-1\linewidth}
\setlength{\colwidth}{26cm}
\renewcommand{\arraystretch}{2}
% \newcommand{\separatorcolumn}{\begin{column}{\sepwidth}\end{column}}

% ====================
% Title
% ====================

\title{Dynamical Analysis of Laser Rate Equations}

\author{Ayush Shenoy, Anuradha Namjoshi, Sukruta Pethe}

\institute[shortinst]{Department of Mathematics, Sathaye College}

% ====================
% Footer (optional)
% ====================

% \footercontent{
%   \href{https://gitlab.com/ashenoy463/frpdynamics
% }{https://gitlab.com/ashenoy463/frpdynamics
% } \hfill
%   IIRC 2022-23 \hfill
%   \href{mailto:ayush.shenoy92@gmail.com}{ayush.shenoy92@gmail.com}}
% % (can be left out to remove footer)

% ====================
% Logo (optional)
% ====================

% use this to include logos on the left and/or right side of the header:
% \logoright{\includegraphics[height=7cm]{logo1.pdf}}
% \logoleft{\includegraphics[height=7cm]{logo2.pdf}}

% ====================
% Body
% ====================

\begin{document}

% Refer to https://github.com/k4rtik/uchicago-poster
% logo: https://www.cam.ac.uk/brand-resources/about-the-logo/logo-downloads
% \addtobeamertemplate{headline}{}
% {
%     \begin{tikzpicture}[remember picture,overlay]
%       \node [anchor=north west, inner sep=3cm] at ([xshift=-2.5cm,yshift=2.75cm]current page.north west)
%       {\includegraphics[height=4.5cm]{logos/unott-logo.eps}}; 
%     \end{tikzpicture}
% }

\begin{frame}[t]
\begin{columns}[t]

\begin{column}{\colwidth}

  \begin{block}{Introduction}

  We model laser systems through photon rate equations. Using techniques from dynamical systems, we can establish the qualitative behaviour of
  the system without obtaining analytical solutions. We then numerically solve the equations to study the transient characteristics.
  
  \end{block}
  
    \begin{exampleblock}{System}
    
    \setlength{\jot}{1em}    
    \begin{align*}
         \frac{dn}{dt} &= -Kn\varphi - Tn + R\\
         \frac{d\varphi}{dt}  &= Kn\varphi - \delta\varphi
    \end{align*}
    
    A system of coupled non-linear differential equations describing
    photon generation and annihilation in the optical cavity.

    \heading{Variables}

    \begin{tabular}{rl}
        $n$ :& number of atoms in upper laser level\\ 
        $\varphi$ :& number of photons in the cavity
    \end{tabular}

    \heading{Parameters}

    \begin{tabular}{rl}
        $R$ :& Pumping rate\\ 
        $T$ :& Rate constant for spontaneous emission\\ 
        $K$ :& Gain coefficient\\
        $\delta$ :& Reciprocal of mean lifetime in upper level\\
    \end{tabular}

  \end{exampleblock}

  \begin{block}{Methods}
  \begin{itemize}\setlength\itemsep{1em}
      \item Find fixed points
      \item Derive linearizations
      \item Examine eigenvalues to determine stability
      \item Draw phase portraits
      \item Study transient behaviour for different parameter regimes 
      \item Interpret results
  \end{itemize}
  \end{block}

    \begin{block}{Equilibria}

        We find two distinct fixed points:
        
        \setlength{\jot}{1em}
        \begin{align*}
            \boldsymbol{x}^{*}_{1} &= \bigg( \frac{\delta}{K}\ ,\ \frac{R}{\delta} - \frac{T}{K} \bigg)\\
            \boldsymbol{x}^{*}_{2} &= \bigg( \frac{R}{T}\ ,\ 0 \bigg)
        \end{align*}

        For $R=R_{t}:=T\delta/K$, both points are equal. We call $R_{t}$ the threshold pumping rate. In that case:

        $$
            \boldsymbol{x}^{*}_{1} = \boldsymbol{x}^{*}_{2} = \boldsymbol{x}^{*}_{t} := \bigg( \frac{\delta}{K}\ ,\ 0 \bigg)  
        $$

        We obtain the following linearizations:

`       $$
        \boldsymbol{J}(\boldsymbol{x}^{*}_{1}) = \mqty( - KR/\delta\ \ &\ \ -\delta \\ KR/\delta - T\ \ &\ \ 0 )
        $$      
        
        $$
            \boldsymbol{J}(\boldsymbol{x}^{*}_{2}) = \mqty( - T\ \ &\ \ -KR/T \\ 0\ \ &\ \ KR/T-\delta )
        $$

        By calculating the eigenvalues of the Jacobian matrices, we conclude that the fixed points cannot be centers or sources as $K,T,R,\delta$ are all positive.

        Through the same methods, we can conclude that $\boldsymbol{x}^{*}_{1}$ and $\boldsymbol{x}^{*}_{2}$ can only be nodal sinks, spiral sinks or saddles depending on the relative magnitude of the coefficients. 
    

    \end{block}

\end{column}

\begin{column}{\colwidth}

    \begin{block}{Bifurcation}
    
        We find that fixed points switch stability at $R=R_{t}$ i.e. a trans-critical bifurcation:
        
        \begin{tabular}{rl}
            $\boldsymbol{x}^{*}_{1}$ :& Saddle $\to$ Nodal/Spiral Sink\\ 
            $\boldsymbol{x}^{*}_{2}$ :& Nodal Sink $\to$ Saddle \\ 
            $\boldsymbol{x}^{*}_{t}$ :& Semi-Stable\\
        \end{tabular}

        Bifurcation determines dynamics around threshold:

        \begin{itemize}\setlength\itemsep{1em}
            \item $R<R_{t}$
            \vspace{1em}\begin{itemize}\setlength\itemsep{1em}
                \item $\boldsymbol{x}^{*}_{2}$ stable; pulls trajectories to $\varphi=0$
                \item $\boldsymbol{x}^{*}_{1}$ is invalid
            \end{itemize}    
            \item $R>R_{t}$
             \vspace{1em}\begin{itemize}\setlength\itemsep{1em}
                \item $\boldsymbol{x}^{*}_{2}$ unstable; saddle pushes trajectories to $\boldsymbol{x}^{*}_{1}$ 
                \item $\boldsymbol{x}^{*}_{1}$ stable; pulls trajectories to $\varphi>0$
            \end{itemize} 
        \end{itemize}
        
        \begin{figure}[!htb]
            \vspace{1em}
            \begin{subfigure}[c]{1.0\linewidth}\label{fig:threshold_at}
                \hspace{6em}\input{figures/threshold_at.tikz}
                \vspace{1em}
                \caption{$R \leq R_{t}$}
            \end{subfigure}
            \vspace{2em}
            \begin{subfigure}{1.0\linewidth}\label{fig:threshold_after}
                \hspace{6em}\input{figures/threshold_after.tikz}
                \vspace{1em}
                \caption{$R>R_{t}$}
            \end{subfigure}
            \caption{The transcritical bifurcation at $R=R_{t}$ and its aftermath}
            \label{fig:threshold_bifurcation}
        \end{figure}
    
    \end{block}

    \begin{block}{Phase Portraits}

        \begin{figure}[!htb]
            \begin{subfigure}{.45\linewidth}
                \includegraphics[scale=0.85]{plates/global_pre.png}
                \caption{$R<R_{t}$}
                \label{fig:global_pre}
            \end{subfigure}
            % \hspace{2em}
            \begin{subfigure}{.45\linewidth}
                \includegraphics[scale=0.85]{plates/global_threshold.png}
                \caption{$R=R_{t}$}
                \label{fig:global_threshold}
            \end{subfigure}
            \par\bigskip
            \begin{subfigure}{.45\linewidth}
                \includegraphics[scale=0.85]{plates/global_post_node.png}
                \caption{$R>R_{t}$, $\boldsymbol{x}^{*}_{1}$ nodal sink}
                \label{fig:global_post_node}
            \end{subfigure}
            % \hspace{2em}
            \begin{subfigure}{.45\linewidth}
                \includegraphics[scale=0.85]{plates/global_post_spiral.png}
                \caption{$R>R_{t}$, $\boldsymbol{x}^{*}_{1}$ spiral sink}
                \label{fig:global_post_spiral}
            \end{subfigure}
            \caption{Global phase portraits of the system for various parameter regimes}
            \label{fig:global_portraits}
        \end{figure}
        % \centering
        % \begin{tabular}{rl}\
        %     (a) :& $K=1,T=25,R=0,\delta=5$\\
        %     (b) :& $K=1,T=25,R=125,\delta=5$\\
        %     (c) :& $K=1,T=25,R=500,\delta=5$\\
        %     (d) :& $K=1,T=25,R=5000,\delta=5$
        % \end{tabular}

    \end{block}

\end{column}

\begin{column}{\colwidth}

  \begin{block}{Transient Behaviour}

    \begin{figure}[!htb]
        \begin{subfigure}{0.5\linewidth}
            \hspace{-1.5em}\includegraphics[scale=1]{plates/relaxation_1.png}
            \caption{$R=50$}
            \label{fig:relaxation_1}
        \end{subfigure}
        \hspace{2em}
        \begin{subfigure}{0.5\linewidth}
            \hspace{-1.5em}\includegraphics[scale=1]{plates/relaxation_2.png}
            \caption{$R=500$}
            \label{fig:relaxation_2}
        \end{subfigure}
        \caption{Relaxation oscillations for $K=5,T=0.01,\delta=100$ ($R_{t} = 1/5$) with the initial conditions $\boldsymbol{x}_{0} = (45,0)$ }
        \label{fig:relaxation_oscillations}
    \end{figure}

    \begin{figure}[!htb]
    % \begin{subfigure}{.5\linewidth}
    %     \includegraphics[scale=1]{plates/nodal_1.png}
    %     \caption{$R=15<R_{t}$}
    %     \label{fig:nodal_1}
    % \end{subfigure}
    % \hspace{2em}
    \begin{subfigure}{.5\linewidth}
        \hspace{-1.5em}\includegraphics[scale=1]{plates/nodal_2.png}
        \caption{$R=30>R_{t}$}
        \label{fig:nodal_2}
    \end{subfigure}
    \caption{Nodal relaxation to equilibrium for $K=2,T=5,\delta=10$ ($R_{t} = 25$) the initial conditions $\boldsymbol{x}_{0}=(25,0)$ }
        \label{fig:nodal_transients}
    \end{figure}

  \end{block}

    \begin{block}{Physical Interpretation}
        \begin{itemize}\setlength\itemsep{1em}
            \item Under the threshold ($R<R_{t}$), $\varphi \to 0$; there is no output in steady state
            \item Over the threshold ($R>R_{t}$), $\varphi>0$ in steady state; we have laser output
            \item When $\boldsymbol{x}^{*}_{1}$ is a spiral sink over the threshold, we get relaxation oscillations
        \end{itemize}
    \end{block}

  \begin{block}{References}
   
      \nocite{datseris2022,haken1986,hirsch2013,milonni2010,strogatz2015,thyagarajan2010}
    \footnotesize{\bibliographystyle{plain}\bibliography{poster}}

  \end{block}

\end{column}

\end{columns}
\end{frame}

\end{document}







