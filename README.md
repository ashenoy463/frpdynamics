# Applications of Dynamical Systems Theory

Notes, codes and report for the 2022-23 FRP project presented at IIRC 2023 on the dynamics of laser systems, exploring inversion and the threshold condition.

- [Report](report.pdf)  
- [Poster](poster.pdf)

## Directory Structure

```
.
├── notes
│   └── <PDFs of project notes>
├── plots
│   └── <code generated plots>
├── report
│   ├── figures
│   │   └── <tikz diagrams>
│   ├── plates
│   │   └── <plots included in report>
│   ├── refs.bib
│   ├── report.tex
│   ├── poster.tex
│   └── <poster theme files>
├── src
│   └── <plotting code>
├── report.pdf
├── poster.pdf
└── README.md
```

## Useful links

- [pplane and dfield](https://www.cs.unm.edu/~joel/dfield/)
- [juliadynamics](https://juliadynamics.github.io/JuliaDynamics/)
- [hirsch smale and devaney](https://eclass.uoa.gr/modules/document/file.php/MATH626/Morris%20W.%20Hirsch%2C%20Stephen%20Smale%20and%20Robert%20L.%20Devaney%20%28Auth.%29%20-%20Differential%20Equations%2C%20Dynamical%20Systems%2C%20and%20an%20Introduction%20to%20Chaos-Academic%20Press%20%282012%29.pdf)
- [tyagarajan and ghatak](https://link.springer.com/book/10.1007/978-1-4419-6442-7)
