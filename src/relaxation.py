from base import *

params = [
    ((5, 0.01, 50, 100), 5, 50),
    ((5, 0.01, 500, 100), 1, 50),
    ((5, 0.01, 2500, 100), 0.4, 120),
    ((5, 0.01, 5000, 100), 0.4, 120)]


def F(t, y, K, T, R, D): return (-K*y[0]
                                 * y[1] - T*y[0] + R, K*y[0]*y[1] - D*y[1])


t_eval = np.arange(0, 5, 0.001)
for i, param in enumerate(params):
    sol = solve_ivp(F, [0, 5], [45, 0.1], args=param[0], t_eval=t_eval)
    fig, ax = plt.subplots(figsize=(6, 5))
    ax.plot(t_eval, sol.y[0], color="blue", label=r"$n$")
    ax.plot(t_eval, sol.y[1], color="red", label=r"$\varphi$")
    ax.set_xlabel('t')
    ax.set_xlim(0, param[1])
    ax.set_ylim(0, param[2])
    ax.legend()
    save(i+1)
