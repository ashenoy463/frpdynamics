from base import *

params = [((2, 5, 5, 10), "pre"), ((2, 5, 60, 10), "post")]

def F(t, y, K,T,R,D): return (-K*y[0]*y[1] - T*y[0] + R, K*y[0]*y[1] - D*y[1])

t_eval = np.arange(0, 5, 0.001)
for param in params:
    p = param[0]
    name = param[1]
    sol = solve_ivp(F, [0, 5], [25, 0.1], args=p, t_eval=t_eval)
    fig, ax = plt.subplots(figsize=(6, 5))
    ax.plot(t_eval, sol.y[0], color="blue", label=r"$n$")
    ax.plot(t_eval, sol.y[1], color="red", label=r"$\varphi$")
    ax.set_xlabel('t')
    ax.set_xlim(0, 2)
    ax.set_ylim(0, 30)
    ax.legend()
    save(name)
