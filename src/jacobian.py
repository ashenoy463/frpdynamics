from base import *

domain = np.linspace(-dim, dim, density)
x, y = np.meshgrid(domain, domain)

for i, p in enumerate([(1, 1, 0.5, 1),
                      (1, 1, 1, 1),
                      (1, 1, 1.5, 1)]):
    fig, ax = plt.subplots(figsize=(5.5, 5))
    ax.streamplot(x, y, *jacobian_one(x, y, p),
                  color="k",
                  linewidth=1,
                  density=0.5,
                  broken_streamlines=False,
                  arrowstyle='->', arrowsize=1)
    ax.set_xlim(-50, 50)
    ax.set_ylim(-50, 50)
    ax.set_xlabel(r"$n$", fontsize=15)
    ax.set_ylabel(r"$\varphi$", fontsize=15)
    ax.plot(0, 0, marker="o", markeredgecolor="k", color=f"{'k' if i==2 else 'w'}", markersize=10)
    save(i+1)
