from base import *

domain = np.linspace(0, dim, density)
x, y = np.meshgrid(domain, domain)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(17, 5.5))
for (ax, p) in zip([ax1, ax2, ax3], [(5, 1, 10), (10, 1, 10), (15, 1, 10)]):
    ax.streamplot(x, y, *rate_three(x, y, p),
                  color="k",
                  linewidth=1,
                  density=0.5,
                  broken_streamlines=False,
                  arrowstyle='->', arrowsize=1)

    def nullcline(x): return (p[0]+p[1])/(p[2]+p[1])*x
    xnew = np.linspace(0, dim, 300)
    ax.plot(xnew, nullcline(xnew), color="r", linestyle="dashdot")
    at = AnchoredText("$W_p$={0}\n\n$T_{{21}}$={2}\n\n$W_l$={1}".format(
        *p), prop=dict(size=10), frameon=True, loc='upper left')
    at.patch.set_boxstyle("round,pad=0.1,rounding_size=0.2")
    ax.add_artist(at)
    ax.set_xlim(0, 100)
    ax.set_ylim(0, 100)
    ax.fill_between(domain, domain, dim, color='green',
                    alpha=0.5)
    ax.set_xlabel(r"$n_1$", fontsize=15)
    ax.set_ylabel(r"$n_2$", fontsize=15)

save()
