from base import *

domain = np.linspace(0, dim, density)
x, y = np.meshgrid(domain, domain)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(17, 5.5))
for (ax, p) in zip([ax1, ax2, ax3], [(1, 1), (10, 1), (100, 1)]):
    ax.streamplot(x, y, *rate_two(x, y, p),
                  color="k",
                  linewidth=1,
                  density=0.5,
                  broken_streamlines=False,
                  arrowstyle='->', arrowsize=1)

    def nullcline(x): return (p[0]/(p[0]+p[1]))*x
    xnew = np.linspace(0, dim, 300)
    ax.plot(xnew, nullcline(xnew), color="r", linestyle="dashdot")
    at = AnchoredText(f"$W_p$={p[0]}", prop=dict(
        size=10), frameon=True, loc='upper left')
    at.patch.set_boxstyle("round,pad=0.1,rounding_size=0.2")
    ax.add_artist(at)
    ax.fill_between(domain, domain, dim, color='green',
                    alpha=0.5)
    ax.set_xlabel(r"$n_1$", fontsize=15)
    ax.set_ylabel(r"$n_2$", fontsize=15)

save()
