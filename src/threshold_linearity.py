from base import *

K, T, R, D = 0.1, 1, 3, 5

fig, ax = plt.subplots(figsize=(5.5, 5))
ax.plot(np.linspace(0, T*D/K, density), np.zeros(density), color="r")
ax.plot(np.linspace(T*D/K, 100, density),
        np.linspace(T*D/K, 100, density)/D - T/K, color="r")
ax.set_xlim(5, 100)
ax.set_ylim(-2, 20)
ax.set_xlabel(r"$R$", fontsize=15)
ax.set_ylabel(r"$\varphi$", fontsize=15)
ax.axvline(T*D/K, linestyle="--", label="$R=R_t$", color="k")
ax.legend()

save()
