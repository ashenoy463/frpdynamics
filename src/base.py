import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from matplotlib import rc
from matplotlib.offsetbox import AnchoredText
import inspect
import os
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

# Co-ordinate space

dim = 100
density = 1000

# Save figures

def save(i=""):
    file = f"{os.path.split(inspect.stack()[1].filename)[1][:-3]}{'_'+str(i) if i!='' else str(i)}.png"
    plt.margins(.1, .1)
    plt.tight_layout()
    plt.savefig(f"{os.path.join(os.path.dirname(os.path.dirname(__file__)), 'plots')}/{file}", dpi=300)
    print(f"[PRINT FIG] => {file}")

# Systems and equations

def threshold(x, y, p):
    """
    Photon Rate Equations (Strogatz)
    p = (K,T,R,δ)

    dn/dt = -Knφ - Tn + R
    dφ/dt = Knφ - δφ

    """
    return (-p[0]*x*y - p[1]*x + p[2], p[0]*x*y - [3]*y)

def rate_two(x, y, p):
    """
    2 Level Population Rate Equations
    p = (Wp,T)

    dn1/dt = -(Wp)n1 + (Wl+T)n2
    dn2/dt = +(Wp)n1 - (Wl+T)n2

    """
    return (-p[0]*x + (p[0]+p[1])*y, p[0]*x - (p[0]+p[1])*y)


def rate_three(x, y, p):
    """
    Constrained 3 Level Population Rate Equations (Milonni & Eberly p.154)
    p = (Wp,Wl,T)

    dn1/dt = -(Wp+Wl)n1 + (Wl+T)n2
    dn2/dt = +(Wp+Wl)n1 - (Wl+T)n2

    """
    return (-(p[0]+p[1])*x + (p[1]+p[2])*y, (p[0]+p[1])*x - (p[1]+p[2])*y)


def jacobian_one(x, y, p):
    """
    Linearization about fixed point x1* (Report p.15)
    p = (K,T,R,δ)

    dn/dt = -(KR/δ)n - δφ
    dφ/dt = +(KR/δ - T)n
    """
    return (-(p[0]*p[2]/p[3])*x - p[3]*y, (p[0]*p[2]/p[3] - p[1])*x + 0*y)
