from base import *

domain = np.linspace(-dim,dim,density)
x, y = np.meshgrid(domain,domain)

params = [((1,25,0,5),'pre'),
((1,25,125,5),"threshold"),
((1,25,500,5),"post_node"),
((5,0.01,5000,100),"post_spiral")]

fig, ax = plt.subplots(figsize=(5.5, 5))
for i, param in enumerate(params):
    ax.cla()
    p = param[0]
    title = param[1]
    ax.streamplot(x, y, *threshold(x, y,p),
                  color="k",
                  linewidth=1,
                  density=0.5,
                  broken_streamlines=False,
                  arrowstyle='->', arrowsize=1)
    if i>1:
        ax.set_xlim(-100, 100)
        ax.set_ylim(-100, 100)
    else:
        ax.set_xlim(-50, 50)
        ax.set_ylim(-50, 50)
    ax.set_xlabel(r"$n$", fontsize=15)
    ax.set_ylabel(r"$\varphi$", fontsize=15)
    ax.plot(p[3]/p[0], p[2]/p[3] - p[1]/p[0], marker="o", markeredgecolor="k", color=f"{'w' if p[2]<= p[1]*p[3]/p[0] else 'k'}", markersize=10,zorder=50)
    ax.plot(p[2]/p[1], 0, marker="o", markeredgecolor="k", color=f"{'w' if p[2] >= p[1]*p[3]/p[0] else 'k'}", markersize=10)
    plt.axvline(p[3]/p[0],0,linestyle="--",color="r",label=r"$n=\delta/K$")
    ax.legend()
    save(title)
